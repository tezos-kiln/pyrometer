import { BakingRightsP } from "../../rpc/types";

const rights: BakingRightsP = [
  {
    "level": 67030,
    "delegate": "tz1TnEtqDV9mZyts2pfMy6Jw1BTPs4LMjL8M",
    "round": 0,
    "estimated_time": "2024-06-04T15:33:47Z",
    "consensus_key": "tz1TnEtqDV9mZyts2pfMy6Jw1BTPs4LMjL8M"
  },
  {
    "level": 67030,
    "delegate": "tz1aKiShiJWeeTSGzuvYWhZPqvMShgrz9Qy4",
    "round": 1,
    "estimated_time": "2024-06-04T15:33:52Z",
    "consensus_key": "tz1aKiShiJWeeTSGzuvYWhZPqvMShgrz9Qy4"
  },
  {
    "level": 67030,
    "delegate": "tz1XMiZwHpHZ8a1AfwRWKfzLskJgZNyV8PHs",
    "round": 4,
    "estimated_time": "2024-06-04T15:34:19Z",
    "consensus_key": "tz1XMiZwHpHZ8a1AfwRWKfzLskJgZNyV8PHs"
  },
  {
    "level": 67030,
    "delegate": "tz1Zt8QQ9aBznYNk5LUBjtME9DuExomw9YRs",
    "round": 5,
    "estimated_time": "2024-06-04T15:34:32Z",
    "consensus_key": "tz1Zt8QQ9aBznYNk5LUBjtME9DuExomw9YRs"
  },
  {
    "level": 67030,
    "delegate": "tz1epK8fDnc8tUeK6dNwTjiHqrGzX586ozyt",
    "round": 9,
    "estimated_time": "2024-06-04T15:35:44Z",
    "consensus_key": "tz1epK8fDnc8tUeK6dNwTjiHqrGzX586ozyt"
  },
  {
    "level": 67030,
    "delegate": "tz1YtB3Hn6oghVk96vkpZt6PHrfbyRY1ciL3",
    "round": 10,
    "estimated_time": "2024-06-04T15:36:07Z",
    "consensus_key": "tz1YtB3Hn6oghVk96vkpZt6PHrfbyRY1ciL3"
  },
  {
    "level": 67030,
    "delegate": "tz3Q67aMz7gSMiQRcW729sXSfuMtkyAHYfqc",
    "round": 14,
    "estimated_time": "2024-06-04T15:37:59Z",
    "consensus_key": "tz3Q67aMz7gSMiQRcW729sXSfuMtkyAHYfqc"
  },
  {
    "level": 67030,
    "delegate": "tz1RuHDSj9P7mNNhfKxsyLGRDahTX5QD1DdP",
    "round": 18,
    "estimated_time": "2024-06-04T15:40:23Z",
    "consensus_key": "tz1RuHDSj9P7mNNhfKxsyLGRDahTX5QD1DdP"
  },
  {
    "level": 67030,
    "delegate": "tz1cjyja1TU6fiyiFav3mFAdnDsCReJ12hPD",
    "round": 23,
    "estimated_time": "2024-06-04T15:44:08Z",
    "consensus_key": "tz1cjyja1TU6fiyiFav3mFAdnDsCReJ12hPD"
  },
  {
    "level": 67030,
    "delegate": "tz1gBnaS1n7LKqpaRnyBX5MSmamadXXfzNpt",
    "round": 28,
    "estimated_time": "2024-06-04T15:48:43Z",
    "consensus_key": "tz1gBnaS1n7LKqpaRnyBX5MSmamadXXfzNpt"
  },
  {
    "level": 67030,
    "delegate": "tz1TGKSrZrBpND3PELJ43nVdyadoeiM1WMzb",
    "round": 29,
    "estimated_time": "2024-06-04T15:49:44Z",
    "consensus_key": "tz1TGKSrZrBpND3PELJ43nVdyadoeiM1WMzb"
  },
  {
    "level": 67030,
    "delegate": "tz1YAVSJRXGxqu4DSPJKkJpbB4Dn3KgzER3F",
    "round": 31,
    "estimated_time": "2024-06-04T15:51:52Z",
    "consensus_key": "tz1YAVSJRXGxqu4DSPJKkJpbB4Dn3KgzER3F"
  },
  {
    "level": 67030,
    "delegate": "tz1PZY3tEWmXGasYeehXYqwXuw2Z3iZ6QDnA",
    "round": 48,
    "estimated_time": "2024-06-04T16:15:23Z",
    "consensus_key": "tz1PZY3tEWmXGasYeehXYqwXuw2Z3iZ6QDnA"
  }
];

export default rights;