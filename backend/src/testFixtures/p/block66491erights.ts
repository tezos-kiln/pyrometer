import { EndorsingRightsP } from "../../rpc/types";

const rights: EndorsingRightsP = [
  {
    "level": 67023,
    "delegates": [
      {
        "delegate": "tz1WhVphATKAtZmDswYGTPWRjPEGvgNT8CFW",
        "first_slot": 5638,
        "attestation_power": 1,
        "consensus_key": "tz1WhVphATKAtZmDswYGTPWRjPEGvgNT8CFW"
      },
      {
        "delegate": "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
        "first_slot": 567,
        "attestation_power": 6,
        "consensus_key": "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
      },
      {
        "delegate": "tz1TGKSrZrBpND3PELJ43nVdyadoeiM1WMzb",
        "first_slot": 437,
        "attestation_power": 31,
        "consensus_key": "tz1TGKSrZrBpND3PELJ43nVdyadoeiM1WMzb"
      },
      {
        "delegate": "tz1KiriVKqPKD4vJQuAJCmtyuT6oYrg4zaaW",
        "first_slot": 226,
        "attestation_power": 8,
        "consensus_key": "tz1KiriVKqPKD4vJQuAJCmtyuT6oYrg4zaaW"
      },
      {
        "delegate": "tz1V1Lu93CPgeN6AXM3Ba9oreAAxcJCoYNJN",
        "first_slot": 137,
        "attestation_power": 34,
        "consensus_key": "tz1V1Lu93CPgeN6AXM3Ba9oreAAxcJCoYNJN"
      },
      {
        "delegate": "tz1RuHDSj9P7mNNhfKxsyLGRDahTX5QD1DdP",
        "first_slot": 49,
        "attestation_power": 332,
        "consensus_key": "tz1RuHDSj9P7mNNhfKxsyLGRDahTX5QD1DdP"
      },
      {
        "delegate": "tz1epK8fDnc8tUeK6dNwTjiHqrGzX586ozyt",
        "first_slot": 32,
        "attestation_power": 310,
        "consensus_key": "tz1epK8fDnc8tUeK6dNwTjiHqrGzX586ozyt"
      },
      {
        "delegate": "tz1Zt8QQ9aBznYNk5LUBjtME9DuExomw9YRs",
        "first_slot": 26,
        "attestation_power": 335,
        "consensus_key": "tz1Zt8QQ9aBznYNk5LUBjtME9DuExomw9YRs"
      },
      {
        "delegate": "tz1cjyja1TU6fiyiFav3mFAdnDsCReJ12hPD",
        "first_slot": 16,
        "attestation_power": 294,
        "consensus_key": "tz1cjyja1TU6fiyiFav3mFAdnDsCReJ12hPD"
      },
      {
        "delegate": "tz1XMiZwHpHZ8a1AfwRWKfzLskJgZNyV8PHs",
        "first_slot": 14,
        "attestation_power": 334,
        "consensus_key": "tz1XMiZwHpHZ8a1AfwRWKfzLskJgZNyV8PHs"
      },
      {
        "delegate": "tz1gBnaS1n7LKqpaRnyBX5MSmamadXXfzNpt",
        "first_slot": 12,
        "attestation_power": 344,
        "consensus_key": "tz1gBnaS1n7LKqpaRnyBX5MSmamadXXfzNpt"
      },
      {
        "delegate": "tz1aKiShiJWeeTSGzuvYWhZPqvMShgrz9Qy4",
        "first_slot": 7,
        "attestation_power": 326,
        "consensus_key": "tz1aKiShiJWeeTSGzuvYWhZPqvMShgrz9Qy4"
      },
      {
        "delegate": "tz3Q67aMz7gSMiQRcW729sXSfuMtkyAHYfqc",
        "first_slot": 4,
        "attestation_power": 376,
        "consensus_key": "tz3Q67aMz7gSMiQRcW729sXSfuMtkyAHYfqc"
      },
      {
        "delegate": "tz1YAVSJRXGxqu4DSPJKkJpbB4Dn3KgzER3F",
        "first_slot": 3,
        "attestation_power": 323,
        "consensus_key": "tz1YAVSJRXGxqu4DSPJKkJpbB4Dn3KgzER3F"
      },
      {
        "delegate": "tz1YtB3Hn6oghVk96vkpZt6PHrfbyRY1ciL3",
        "first_slot": 2,
        "attestation_power": 329,
        "consensus_key": "tz1YtB3Hn6oghVk96vkpZt6PHrfbyRY1ciL3"
      },
      {
        "delegate": "tz1PZY3tEWmXGasYeehXYqwXuw2Z3iZ6QDnA",
        "first_slot": 1,
        "attestation_power": 346,
        "consensus_key": "tz1PZY3tEWmXGasYeehXYqwXuw2Z3iZ6QDnA"
      },
      {
        "delegate": "tz1TnEtqDV9mZyts2pfMy6Jw1BTPs4LMjL8M",
        "first_slot": 0,
        "attestation_power": 3271,
        "consensus_key": "tz1TnEtqDV9mZyts2pfMy6Jw1BTPs4LMjL8M"
      }
    ]
  }
];

export default rights;